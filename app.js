const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
var http = require('https');
var app = express();

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.get('/minhaapi/', function (req, res) {

   var options = {
        "method": "GET",
        "hostname": "http://169.254.169.254",
        "port": null,
        "path": "/latest/meta-data/instance-id"
   };

   var req = http.request(options, function (res) {
        var chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            var body = Buffer.concat(chunks);
            response.sendStatus(200);
        });
    });
   
    req.end();

});

var host = (process.env.VCAP_APP_HOST || 'localhost');
var port = (process.env.VCAP_APP_PORT || 3000);
app.listen(port, host);
